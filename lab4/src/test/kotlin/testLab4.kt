package org.example.lab4

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class testLab4 {

    @Test
    fun `2 + 2 = 4`() {
        assertEquals(4, add(2, 2), "2 + 2 should equal 4")
    }
}
